import java.util.Scanner;

public class MainClass {
    private static final int ROWS = 4;
    private static final int COLS = 4;

    public static void main(String[] arg) {
        menu();
    }

    private static void menu() {
        Scanner in = new Scanner(System.in);
        System.out.println("\t\t\t\t\t******");
        System.out.println("*********************2048*********************");
        System.out.println("\t\t\t\t\t******");
        System.out.println("\n1.Start\n2.Exit\n\n");
        while(true) {
            System.out.println("Enter (1) for start game or (2) for exit game");
            String key = in.next();
            if(key.equals("1")) {
                start();
                break;
            }
            else if(key.equals("2")) {
                System.exit(0);
            }
        }
    }
    private static int target = 2048;
    //get input over and over until game over
    private static void start() {
        target = 2048;
        short [][] board = new short[ROWS][COLS];
        int score = 0, step = 0;
        RandomElementٍ(board);
        while(true) {
            RandomElementٍ(board);
            print(board, score, step);
            boolean end;
            short [][] temp = new short[ROWS][COLS];

            do {
                copyArray(board, temp);
                end = checkOver(board, temp);
                if(!end) {
                    break;
                }
                copyArray(board, temp);
                char key = input();
                if (key == 'd' || key == 'D') {
                    score += actionRight(board);
                } else if (key == 'a' || key == 'A') {
                    score += actionLeft(board);
                } else if (key == 'w' || key == 'W') {
                    score += actionUp(board);
                } else if (key == 's' || key == 'S') {
                    score += actionDown(board);
                }
            }while (!(isChange(board, temp)));
            step++;
            if (reachTarget(board)) {
                print(board, score, step);
                closeProgram();
            }
            else if(!end) {
                System.out.println("You lose!");
                menu();
            }
        }
    }
    //get input from user
    private static char input() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a-A or w-W or d-D or s-S then press Enter:");
        char key;
        while(true) {
            char ch = in.next().charAt(0);
            if (ch == 'a' || ch == 'A' || ch == 'd' || ch == 'D' || ch == 'w' || ch == 'W' || ch == 's' || ch == 'S') {
                key = ch;
                break;
            }
            System.out.println("Invalid input!");
            System.out.println("Enter a-A or w-W or d-D or s-S then press Enter:");
        }
        return key;
    }
    //put (2) in random a empty element of array randomly
    private static void RandomElementٍ(short[][] board) {
        while(true) {
            int row = (int) (Math.random() * 10);
            if(row > ROWS * 2) {
                continue; // for justice
            }
            row %= ROWS;
            int col = (int) (Math.random() * 10);
            if(col > ROWS * 2) {
                continue; // for justice
            }
            col %= COLS;
            if(!(isEmpty(board, row, col))) {
                continue;
            }
            board[row][col] = 2;
            break;
        }
    }
    //copy board in another array
    private static void copyArray(short[][] board, short[][] temp) {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                temp[i][j] = board[i][j];
            }
        }
    }
    //move element to left
    private static void moveLeft(short[][] board) {
        for (int i = 0; i < ROWS; i++) {
            for (int k = 0; k < ROWS - 1; k++) {
                for (int j = 1; j < COLS; j++) {
                    if (board[i][j - 1] == 0) {
                        board[i][j - 1] = board[i][j];
                        board[i][j] = 0;
                    }
                }
            }
        }
    }

    private static int actionLeft(short[][] board) {
        moveLeft(board);
        int score = 0;
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS - 1; j++) {
                if (board[i][j] >= 2 && (board[i][j] == board[i][j + 1])) {
                    board[i][j] *= 2;
                    score += board[i][j + 1];
                    board[i][j + 1] = 0;
                    j++;
                }
            }
        }
        moveLeft(board);
        return score;
    }
    private static void moveRight(short[][] board) {
        for (int i = 0; i < ROWS; i++) {
            for (int k = 0; k < ROWS - 1; k++) {
                for (int j = COLS - 2; j >= 0; j--) {
                    if (board[i][j + 1] == 0) {
                        board[i][j + 1] = board[i][j];
                        board[i][j] = 0;
                    }
                }
            }
        }
    }

    private static int actionRight(short[][] board) {
        int score = 0;
        moveRight(board);
        for (int i = 0; i < ROWS; i++) {
            for (int j = COLS - 1; j > 0; j--) {
                if (board[i][j] >= 2 && (board[i][j] == board[i][j - 1])) {
                    board[i][j] *= 2;
                    score += board[i][j - 1];
                    board[i][j - 1] = 0;
                    j--;
                }
            }
        }
        moveRight(board);
        return score;
    }
    private static void moveUp(short[][] board) {
        for(int j = 0; j < COLS; j++) {
            for(int k = 0; k < ROWS - 1; k++) {
                for (int i = 1; i < ROWS; i++) {
                    if(board[i - 1][j] == 0) {
                        board[i - 1][j] = board[i][j];
                        board[i][j] = 0;
                    }
                }
            }
        }
    }

    private static int actionUp(short[][] board) {
        int score = 0;
        moveUp(board);
        for(int j = 0; j < COLS; j++) {
            for(int i = 0; i < ROWS - 1; i++) {
                if(board[i][j] >= 2 && (board[i][j] == board[i + 1][j])) {
                    board[i][j] *= 2;
                    score += board[i + 1][j];
                    board[i + 1][j] = 0;
                    i++;
                }
            }
        }
        moveUp(board);
        return score;
    }

    private static void moveDown(short[][] board) {
        for(int j = 0; j < COLS; j++) {
            for(int k = 0; k < ROWS - 1; k++) {
                for(int i = ROWS - 2; i >= 0; i--) {
                    if (board[i + 1][j] == 0) {
                        board[i + 1][j] = board[i][j];
                        board[i][j] = 0;
                    }
                }
            }
        }
    }

    private static int actionDown(short[][] board) {
        int score = 0;
        moveDown(board);
        for(int j = 0; j < COLS; j++) {
            for(int i = ROWS - 1; i > 0; i--) {
                if(board[i][j] >= 2 && (board[i][j] == board[i - 1][j])) {
                    board[i][j] *= 2;
                    score += board[i - 1][j];
                    board[i - 1][j] = 0;
                    i--;
                }
            }
        }
        moveDown(board);
        return score;
    }
    //check that element of array is empty
    private static boolean isEmpty(short[][] board, int row, int col) {
        if(board[row][col] < 2) {
            return true;
        }
        return false;
    }
    //check for game over
    private static boolean checkOver(short[][] board, short[][] temp) {
        actionUp(temp);
        if(isChange(board,temp)) {
            return true;
        }
        actionDown(temp);
        if(isChange(board,temp)) {
            return true;
        }
        actionRight(temp);
        if(isChange(board, temp)) {
            return true;
        }
        actionLeft(temp);
        if (isChange(board, temp)) {
            return true;
        }
        return false;
    }
    public static boolean isChange(short[][] board, short[][] temp) {
        for(int i = 0; i < ROWS; i++) {
            for(int j = 0; j < COLS; j++) {
                if (board[i][j] != temp[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }
    //print board
    private static void print(short[][] board, int score, int step) {
        System.out.println("\n######################################################\n");
        System.out.println("score: " + score + "\t\t\t\t\t\t\t\tsteps: " + step);
        printTableLine();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (board[i][j] == 0) {
                    System.out.print("|\t\t\t");
                }
                else {
                    System.out.printf("|\t%4d\t", board[i][j]);
                }
            }
            System.out.println("|");
            printTableLine();
        }
    }
    private static void printTableLine() {
        System.out.print("+");
        for(int i = 0; i < COLS; i++) {
            System.out.print("-----------+");
        }
        System.out.println();
    }
    static boolean reachTarget(short[][] board) {
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                if (board[i][j] == target) {
                    target *= 2;
                    return true;
                }
            }
        }
        return false;
    }
    private static void closeProgram() {
        System.out.println("You win!\nContinue? \"for go to menu press (y) for continue press another key\"");
        Scanner in = new Scanner(System.in);
        String key = in.next();
        if(key.equals("y") || key.equals("Y")) {
            menu();
        }
        System.out.println("New target: " + target);
    }
}